#include <iostream>

using namespace std;

typedef int type;
class Stack
{
public:
	Stack (int new_size) : top(-1)
	{
		size = new_size;
		arr = new type[size];
	}
	~Stack()						//����������
	{
		delete[] arr;
	}
	bool isEmpty() const			//��������� ������ �� ����
	{
		return top == -1;
	}
	type get() const				//���������� ������� �� ������� �����
	{
		return arr[top];
	}

	void pop()						//���������� ���������� ��������
	{
		if (top >= 0)
		{
			top--;
		}
	}

	void push(type item)			//��������� ����� ������� � ����
	{
		if (top == size - 1)
		{
			resize(2 * size);
		}
		arr[++top] = item;
	}
	
private:
	type* arr;
	int size;
	int top;

	void resize(int nsize)
	{
		type* temp = new type[nsize];
		for (int i = 0; i < size; i++)
		{
			temp[i] = arr[i];
		}
		delete[] arr;
		arr = temp;
		size = nsize;
	}
};

int main()
{
	setlocale(LC_ALL, "ru");

	Stack s(4);
	s.push(5);
	s.push(7);
	s.push(11);
	s.push(16);
	s.push(29);
	s.push(34);

	cout << s.get() << endl;
	s.pop();
	cout << s.get() << endl;
	s.pop();
	cout << s.get() << endl;
	s.pop();
	cout << s.get() << endl;
	s.pop();
	cout << s.get() << endl;
	s.pop();
	cout << s.get() << endl;
	s.pop();
}